-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Май 30 2018 г., 11:03
-- Версия сервера: 10.1.31-MariaDB
-- Версия PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `playroom`
--

-- --------------------------------------------------------

--
-- Структура таблицы `inkas`
--

CREATE TABLE `inkas` (
  `id` int(11) NOT NULL,
  `gumar` int(128) NOT NULL,
  `mutq_elq` varchar(10) NOT NULL,
  `amsativ` varchar(255) NOT NULL,
  `coment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `kassa`
--

CREATE TABLE `kassa` (
  `id` int(11) NOT NULL,
  `kassa` varchar(15) NOT NULL,
  `balance` int(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `kassa`
--

INSERT INTO `kassa` (`id`, `kassa`, `balance`) VALUES
(1, 'charbax', 36115);

-- --------------------------------------------------------

--
-- Структура таблицы `pahest`
--

CREATE TABLE `pahest` (
  `id` int(11) NOT NULL,
  `apranq` varchar(30) NOT NULL,
  `qanak` int(5) NOT NULL,
  `gin` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pahest`
--

INSERT INTO `pahest` (`id`, `apranq`, `qanak`, `gin`) VALUES
(1, 'Coca-cola', 35, 200),
(2, 'Fanta', 10, 200),
(3, 'Sprite', 9, 200),
(4, 'FuzTea mec', 8, 300),
(5, 'Kilikia', 15, 350),
(6, 'Siser', 14, 200),
(7, 'Snickers', 10, 250),
(8, 'Mars', 9, 250),
(9, 'Nesquik', 10, 200),
(10, 'Nuts', 10, 200),
(11, 'Milky-Way', 9, 200),
(12, 'Twix', 8, 250),
(13, 'M\'&M\'s', 12, 300),
(14, 'Bounty', 8, 250),
(15, 'KitKat', 15, 250),
(16, 'Dobri sok', 12, 350),
(17, 'Sary Surch', 30, 150),
(18, 'Jur bnakan', 8, 150),
(19, 'FuzTea poqr', 10, 200);

-- --------------------------------------------------------

--
-- Структура таблицы `play_station`
--

CREATE TABLE `play_station` (
  `id` int(11) NOT NULL,
  `game` varchar(30) NOT NULL,
  `skizb` varchar(128) NOT NULL,
  `gumar` int(10) NOT NULL,
  `jamanak` int(10) NOT NULL,
  `azat` int(1) NOT NULL DEFAULT '0',
  `hashiv` int(12) NOT NULL DEFAULT '0',
  `patver` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `play_station`
--

INSERT INTO `play_station` (`id`, `game`, `skizb`, `gumar`, `jamanak`, `azat`, `hashiv`, `patver`) VALUES
(1, 'ps1', '1527633820', 83, 10, 1, 0, ''),
(2, 'ps2', '1527640125', 500, 0, 1, 0, ''),
(3, 'ps3', '1527633751', 200, 24, 1, 0, ''),
(4, 'ps4', '1527638196', 500, 0, 1, 600, ''),
(5, 'biliard', '1527633900', 1500, 0, 1, 0, '');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `inkas`
--
ALTER TABLE `inkas`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `kassa`
--
ALTER TABLE `kassa`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pahest`
--
ALTER TABLE `pahest`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `play_station`
--
ALTER TABLE `play_station`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `inkas`
--
ALTER TABLE `inkas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `kassa`
--
ALTER TABLE `kassa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `pahest`
--
ALTER TABLE `pahest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблицы `play_station`
--
ALTER TABLE `play_station`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
