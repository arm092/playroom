$(document).ready(function() {
    $('.image').on({
        click: gameOptions,
        contextmenu: selectThis
    })
    $('#report').on('dblclick', function() {
        $('.report').fadeIn()
        $('.psb').on('click', function() {
            $('#report-table').html("<tr><th>Անվանում</th><th>Արժեք</th><th>Քանակ</th><th>Գումար</th><th>Նշումներ</th></tr>")
            $.getJSON('scripts/log.php', {}, function(res) {
                for (let i = 0; i < res.log.length; i++) {
                    let tr = $('<tr></tr>')
                    if (res.log[i].service == 'ps1' || res.log[i].service == 'ps2' || res.log[i].service == 'ps3' || res.log[i].service == 'ps3' || res.log[i].service === 'ps4' || res.log[i].service == 'biliard') {
                        for (const key in res.log[i]) {
                            if (key == 'id') continue
                            let td = $('<td></td>')
                            td.text(res.log[i][key])
                            tr.append(td)
                        }
                        $('#report-table').append(tr)
                    }
                }
            })
        })
        $('.other').on('click', function() {
            $('#report-table').html("<tr><th>Անվանում</th><th>Արժեք</th><th>Քանակ</th><th>Գումար</th><th>Նշումներ</th></tr>")
            $.getJSON('scripts/log.php', {}, function(res) {
                for (let i = 0; i < res.log.length; i++) {
                    let tr = $('<tr></tr>')
                    if (res.log[i].service == 'ps1' || res.log[i].service == 'ps2' || res.log[i].service == 'ps3' || res.log[i].service == 'ps3' || res.log[i].service === 'ps4' || res.log[i].service == 'biliard') {
                        continue
                    }
                    for (const key in res.log[i]) {
                        if (key == 'id') continue
                        let td = $('<td></td>')
                        td.text(res.log[i][key])
                        tr.append(td)
                    }
                    $('#report-table').append(tr)
                }
            })
        })
        $('.all').on('click', function() {
            $('#report-table').html("<tr><th>Ամսաթիվ</th><th>Անվանում</th><th>Արժեք</th><th>Քանակ</th><th>Գումար</th><th>Նշումներ</th></tr>")
            $.getJSON('scripts/log.php', {}, function(res) {
                for (let i = 0; i < res.log.length; i++) {
                    let tr = $('<tr></tr>')
                    for (const key in res.log[i]) {
                        if (key == 'id') continue
                        let td = $('<td></td>')
                        if (key == 'time') {
                            let time = new Date(res.log[i][key] * 1000);
                            let timeTxt = `${time.getDate()}.${time.getMonth()+1}.${time.getFullYear()} ${time.getHours()}:${time.getMinutes()}:${time.getSeconds()}`;
                            td.text(timeTxt);
                        } else {
                            td.text(res.log[i][key])
                        }
                        tr.append(td)
                    }
                    $('#report-table').append(tr)
                }
            })
        })
    })
    let pahest;
    $.getJSON('scripts/get.php', {}, function(res) {
        pahest = {};
        $('.money').text(+res.result);
        $('#mterq').empty()
        $('#qanak').val(1)
        for (const elem of res.pahest) {
            let option = $('<option></option>')
            option.prop('value', elem.apranq)
            let name = $('<span></span>')
            name.text(elem.apranq + ': ' + elem.qanak + ' հատ')
            pahest[elem.apranq] = elem.qanak
            option.append(name)
            $('#mterq').append(option)
        }
        for (const elem of res.ps) {
            if (elem.azat == 0) {
                $('#' + elem.game).off('click').prop('title', elem.patver)
                let time = new Date(+elem.skizb * 1000)
                let hours = time.getHours()
                let mins = time.getMinutes()
                let nowHours = new Date().getHours()
                let nowMins = new Date().getMinutes()
                if (elem.jamanak > 0) {
                    let deltaMins = +elem.jamanak - (nowMins - mins + (nowHours - hours) * 60)
                    let h = parseInt(deltaMins / 60)
                    let m = deltaMins - h * 60
                    if (m < 0) {
                        h--
                        m = 60 - m
                        if (h < 0) {
                            endThis(elem.game, 0)
                        }
                    }
                    let xh = ''
                    let xm = ''
                    $('#' + elem.game).text(`0${h}:${m}`);
                    let timer = setInterval(function() {
                        m--;
                        if (m < 0) {
                            m = 59;
                            h--
                        }
                        if (h < 0) {
                            endThis(elem.game, timer)
                        } else {
                            xh = h < 10 ? '0' + h : h;
                            xm = m < 10 ? '0' + m : m;
                            $('#' + elem.game).text(`${xh}:${xm}`);
                        }
                    }, 60000)
                    $('#' + elem.game).on('dblclick', function() {
                        endThis(elem.game, timer)
                    })
                } else if (elem.jamanak == 0) {
                    let deltaMins = nowMins - mins + (nowHours - hours) * 60
                    let h = parseInt(deltaMins / 60)
                    let m = deltaMins - h * 60
                    let xh = ''
                    let xm = ''
                    $('#' + elem.game).text(`0${h}:${m}`);
                    m++
                    let timer = setInterval(function() {
                        m++;
                        if (m > 59) {
                            m = 0;
                            h++
                        }
                        xh = h < 10 ? '0' + h : h;
                        xm = m < 10 ? '0' + m : m;
                        $('#' + elem.game).text(`${xh}:${xm}`);
                    }, 60000)
                    $('#' + elem.game).on('dblclick', function() {
                        endThis(elem.game, timer)
                    })
                }
            }
        }
    })
    let pool = false;
    let nolim = false;
    let buy = 0;
    let selected = false;

    function selectThis() {
        if ($(this).text() != $(this).attr('id')) {
            $('.image').removeClass('selected');
            if (selected != $(this).attr('id')) {
                $(this).addClass('selected');
                selected = $(this).attr('id');
                $('.sale').off().on('click', addToCart);
            } else {
                $('.sale').off().on('click', sale);
                selected = false
            }
        }
        return false
    }
    $('#buy').on('click', buyButtonOn);

    function buyButtonOn() {
        buy++
        if (buy == 3) {
            $('#buy').attr('data-on', "true").off().one('click', buyProducts)
        }
    }

    function buyProducts() {
        let count = +$('#qanak').val();
        let product = $('#mterq').val();
        $.getJSON('scripts/apranq.php', {
            'sale_count': 0 - count,
            'product': product
        }, function(res) {
            $('.money').text(+res.result);
            $('#mterq').empty()
            $('#qanak').val(1)
            for (const elem of res.pahest) {
                let option = $('<option></option>')
                option.prop('value', elem.apranq)
                let name = $('<span></span>')
                name.text(elem.apranq + ': ' + elem.qanak + ' հատ')
                option.append(name)
                $('#mterq').append(option)
            }
            $('#buy').attr('data-on', false).on('click', buyButtonOn);
            buy = 0;
        })
    }
    if (!selected) {
        $('.sale').off().on('click', sale);
    } else {
        $('.sale').off().on('click', addToCart);
    }

    function sale() {
        let saleCount = +$('#qanak').val();
        let product = $('#mterq').val()
        if (saleCount > pahest[product]) return
        $.getJSON('scripts/apranq.php', {
            'sale_count': saleCount,
            'product': product
        }, function(res) {
            pahest = {}
            $('.money').text(+res.result);
            $('#mterq').empty()
            $('#qanak').val(1)
            for (const elem of res.pahest) {
                let option = $('<option></option>')
                option.prop('value', elem.apranq)
                let name = $('<span></span>')
                name.text(elem.apranq + ': ' + elem.qanak + ' հատ')
                pahest[elem.apranq] = elem.qanak
                option.append(name)
                $('#mterq').append(option)
            }
        })
    }

    function addToCart() {
        let saleCount = +$('#qanak').val();
        let product = $('#mterq').val()
        $.getJSON('scripts/add_to_cart.php', {
            'sale_count': saleCount,
            'product': product,
            'ps': selected
        }, function(res) {
            $('#' + selected).removeClass('selected').prop('title', res.title)
            selected = false
            $('#mterq').empty()
            $('#qanak').val(1)
            for (const elem of res.pahest) {
                let option = $('<option></option>')
                option.prop('value', elem.apranq)
                let name = $('<span></span>')
                name.text(elem.apranq + ': ' + elem.qanak + ' հատ')
                option.append(name)
                $('#mterq').append(option)
            }
            $('.sale').off().on('click', sale);
        })
    }

    $('.inkas').on('click', function() {
        $('#inkas').val(0)
        $('.encash').fadeIn()
        $('.cash').on('click', function() {
            //$(this).off()
            let sum = +$('#inkas').val()
            let type = $('input[type="radio"]:checked').attr('id')
            let coment = $('textarea').val()
            console.log(type)
            if (sum > 0 && (type == 'in' || type == 'out')) {
                $.getJSON('scripts/inkas.php', {
                    'sum': sum,
                    'in_out': type,
                    'coment': coment
                }, function(res) {
                    $('.money').text(+res.result);
                    $('#inkas').val(0)
                    $('textarea').val('')
                    $('.encash').fadeOut()
                    $('.cash').off()
                })
            }
        })
    })
    $('.cancel').click(close)

    function close() {
        $('.mod').fadeOut()
        $('#inkas').val(0)
        $('textarea').val('')
        $('.cash').off()
        $('.options input').val(0);
        $('#no-limit').prop('checked', false)
        $('#sum').prop('disabled', false).off()
        $('#mins').prop('disabled', false).off()
        $('.start').off()
        $('.psb').off()
        $('.other').off()
        $('.all').off()
        nolim = false;
        pool = false;
    }

    function endThis(thisGame, timer) {
        $('#' + thisGame).off('click').off('dblclick')
        clearInterval(timer)
        $.getJSON('scripts/end.php', {
            'ps': thisGame
        }, function(res) {
            $('.money').text(+res.result);
            if (res.sum > 0) {
                $('#' + thisGame).html("Վճարման ենթակա՝<br>" + res.sum + " դր․").one('click', function() {
                    $(this).text(thisGame).on('click', gameOptions).prop('title', '')
                })
            } else {
                $('#' + thisGame).on('click', gameOptions).text(thisGame)
            }
        })
    }

    function gameOptions() {
        if (selected) {
            $.getJSON('scripts/move.php', {
                'from': selected,
                'to': $(this).attr('id')
            }, function(res) {
                $('#' + selected).removeClass('selected')
                location.reload()
            })
        } else {
            let _this = $(this)
            let thisGame = $(this).text()
            pool = thisGame.length > 3 ? true : false
            $('.game').fadeIn().find('.game-name').text(thisGame)
            $('#no-limit').on('change', function() {
                if ($('#no-limit').prop('checked')) {
                    $('#sum').prop('disabled', true)
                    $('#mins').prop('disabled', true)
                    nolim = true
                } else {
                    $('#sum').prop('disabled', false)
                    $('#mins').prop('disabled', false)
                    nolim = false
                }
            })
            $('#sum').on('input', calcMins)
            $('#mins').on('input', calcSum)
            $('.start').on('click', function() {
                if (($('#sum').val() == 0 || $('#mins').val() == 0) && !nolim) return
                $('.start').off()
                $('#' + thisGame).off('click').off('dblclick')
                if (!nolim) {
                    $.getJSON('scripts/play.php', {
                        'sum': +$('#sum').val(),
                        'ps': $('.game-name').text(),
                        'time': +$('#mins').val()
                    }, function(res) {
                        $('.money').text(+res.result);
                        let h = parseInt($('#mins').val() / 60)
                        let m = +$('#mins').val() - h * 60
                        let xh = ''
                        let xm = ''
                        $('#' + res.id).text(`0${h}:${m}`);
                        let timer = setInterval(function() {
                            m--;
                            if (m < 0) {
                                m = 60 + m;
                                h--
                            }
                            if (h < 0) {
                                endThis(thisGame, timer)
                            } else {
                                xh = h < 10 ? '0' + h : h;
                                xm = m < 10 ? '0' + m : m;
                                _this.text(`${xh}:${xm}`);
                            }
                        }, 60000)
                        $('#' + thisGame).on('dblclick', function() {
                            endThis(thisGame, timer)
                        })
                        close()
                    })
                } else {
                    let price = pool ? 1500 : 500
                    $.getJSON('scripts/no_lim.php', {
                        'price': price,
                        'ps': $('.game-name').text()
                    }, function(res) {
                        let h = 0;
                        let m = 1;
                        _this.text(`00:00`);
                        let timer = setInterval(function() {
                            m++;
                            if (m > 59) {
                                m = 0;
                                h++
                            }
                            xh = h < 10 ? '0' + h : h;
                            xm = m < 10 ? '0' + m : m;
                            _this.text(`${xh}:${xm}`);
                        }, 60000)
                        $('#' + thisGame).on('dblclick', function() {
                            endThis(thisGame, timer)
                        })
                        close()
                    })
                }

            })
        }
    }

    function calcMins() {
        let sum = +$('#sum').val()
        let now = new Date().getHours()
        let price = sum == 600 && now < 13 ? 300 : 500
        if (pool) price = 1500
        $('#mins').val((sum / price * 60).toFixed(0))
    }

    function calcSum() {
        let mins = +$('#mins').val()
        let now = new Date().getHours()
        let price = mins == 120 && now < 13 ? 300 : 500
        if (pool) price = 1500
        $('#sum').val((mins / 60 * price).toFixed(0))
    }
})